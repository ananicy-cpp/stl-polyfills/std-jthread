#include <thread>
#include <chrono>
#include <iostream>

int main() {
  std::cout << "Starting main..." << std::endl;
  std::this_thread::sleep_for(std::chrono::milliseconds(50));
  std::jthread a ([] (const std::stop_token& stop_token) -> int {
    std::cout << "Starting thread " << std::this_thread::get_id() << std::endl;
    while (!stop_token.stop_requested()) {
      std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }
    std::cout << "Finished thread " << std::this_thread::get_id() << std::endl;
    return 42;
  });
  std::cout << "Finished main\n";
  std::this_thread::sleep_for(std::chrono::milliseconds(50));
  return EXIT_SUCCESS;
}
