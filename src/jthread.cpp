#include <thread>

namespace std {
  jthread::~jthread() {
    ssource.request_stop();
    th.join();
  }

  jthread::jthread(jthread&& other) noexcept {
    ssource = std::move(other.ssource);
    th = std::move(other.th);
  }

  jthread& jthread::operator=(jthread&& other) noexcept {
    ssource = std::move(other.ssource);
    th = std::move(other.th);
    return *this;
  }

  void jthread::swap(jthread& other) noexcept {
    th.swap(other.th);
    ssource.swap(other.ssource);
  }

  bool jthread::joinable() const noexcept {
    return th.joinable();
  }

  void jthread::join() {
    return th.join();
  }

  void jthread::detach() {
    return th.detach();
  }

  jthread::id jthread::get_id() const noexcept {
    return th.get_id();
  }

  jthread::native_handle_type jthread::native_handle() {
    return th.native_handle();
  }

  stop_source jthread::get_stop_source() noexcept {
    return ssource;
  }

  stop_token jthread::get_stop_token() const noexcept {
    return ssource.get_token();
  }

  bool jthread::request_stop() noexcept {
    return ssource.request_stop();
  }

  void swap(jthread& a, jthread& b) noexcept {
    a.swap(b);
  }

  unsigned int jthread::hardware_concurrency() noexcept {
    return thread::hardware_concurrency();
  }
}
