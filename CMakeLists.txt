cmake_minimum_required(VERSION 3.15)

if(${CMAKE_VERSION} VERSION_LESS "3.21")
    get_directory_property(has_parent PARENT_DIRECTORY)
    set(PROJECT_IS_TOP_LEVEL NOT(has_parent))
endif()

set(version 0.1.0)
set(polyfill_name "jthread")
set(polyfill_name_capitalized "Jthread")

list(APPEND CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake")
include(Helpers)

project(stl_polyfills VERSION ${version} LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 20)
set(CMAKE_CXX_STANDARD_REQUIRED TRUE)

set(CMAKE_MESSAGE_CONTEXT_SHOW ON)

push(CMAKE_MESSAGE_CONTEXT "std")

include(CheckIncludeFileCXX)
include(CheckCXXSymbolExists)
include(CheckCXXCompilerFlag)
include(CheckCXXSourceCompiles)

macro(test_jthread)
    message(STATUS "CMAKE_REQUIRED_FLAGS = ${CMAKE_REQUIRED_FLAGS}")
    check_cxx_source_compiles("
            #include <thread>
            int main () {
                std::jthread a;
                return 0;
            }
    " HAS_STD_JTHREAD)
endmacro()
test_jthread()


message(STATUS "Has CXX ${polyfill_name} support: ${HAS_STD_JTHREAD}")

if (HAS_STD_JTHREAD)
    set(polyfill_lib_type INTERFACE)
else()
    set(polyfill_lib_type STATIC)
endif()

add_library(stl_polyfill_${polyfill_name} ${polyfill_lib_type})
add_library(stl_polyfill::${polyfill_name} ALIAS stl_polyfill_${polyfill_name})

if (NOT(HAS_STD_JTHREAD))
    target_include_directories(stl_polyfill_${polyfill_name} PUBLIC
        $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/polyfills/${polyfill_name}>
        $<INSTALL_INTERFACE:polyfills/${polyfill_name}>)

    target_sources(stl_polyfill_${polyfill_name} PRIVATE
            $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src/jthread.cpp>
            $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/src/stop_source.cpp>)
endif()


set_property(TARGET stl_polyfill_${polyfill_name} PROPERTY INTERFACE_stl_polyfill_${polyfill_name}_MAJOR_VERSION 0)
set_property(TARGET stl_polyfill_${polyfill_name} APPEND PROPERTY COMPATIBLE_INTERFACE_STRING stl_polyfill_${polyfill_name}_MAJOR_VERSION)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
        "${CMAKE_CURRENT_BINARY_DIR}/StlPolyfill${polyfill_name_capitalized}ConfigVersion.cmake"
        VERSION "${version}"
        COMPATIBILITY AnyNewerVersion
)

install(TARGETS stl_polyfill_${polyfill_name}
        EXPORT StlPolyfill${polyfill_name_capitalized}Targets
        LIBRARY DESTINATION lib COMPONENT Runtime
        ARCHIVE DESTINATION lib COMPONENT Development
        RUNTIME DESTINATION bin COMPONENT Runtime
        PUBLIC_HEADER DESTINATION include COMPONENT Development
        BUNDLE DESTINATION bin COMPONENT Runtime)
export(EXPORT StlPolyfill${polyfill_name_capitalized}Targets NAMESPACE stl_polyfills)


include(CMakePackageConfigHelpers)
configure_package_config_file(
        "${PROJECT_SOURCE_DIR}/cmake/StlPolyfill${polyfill_name_capitalized}Config.cmake.in"
        "${PROJECT_BINARY_DIR}/StlPolyfill${polyfill_name_capitalized}Config.cmake"
        INSTALL_DESTINATION lib/cmake/StlPolyfill${polyfill_name_capitalized}
)

install(EXPORT StlPolyfill${polyfill_name_capitalized}Targets
        NAMESPACE stl_polyfills
        DESTINATION lib/cmake/StlPolyfill${polyfill_name_capitalized})
install(FILES
        "${PROJECT_BINARY_DIR}/StlPolyfill${polyfill_name_capitalized}ConfigVersion.cmake"
        "${PROJECT_BINARY_DIR}/StlPolyfill${polyfill_name_capitalized}Config.cmake"
        DESTINATION lib/cmake/StlPolyfill${polyfill_name_capitalized})
install(DIRECTORY ${PROJECT_SOURCE_DIR}/polyfills DESTINATION include)

if (PROJECT_IS_TOP_LEVEL OR TRUE)
    set(stl_polyfill_${polyfill_name}_DIR "${CMAKE_CURRENT_BINARY_DIR}")

    if (BUILD_TESTING)
        add_subdirectory(tests)
    endif ()
endif()

pop(CMAKE_MESSAGE_CONTEXT)
